# std
import json
import os

# lib
from loguru import logger

# app
from env import ORDERS_PATH

def get_all_order_ids():
    return [file.name for file in ORDERS_PATH.glob('*')]

def delete_order(order_id):
    file_name = ORDERS_PATH / str(order_id)
    if file_name.exists():
        os.remove(file_name)

def get_order(order_id):
    file_name = ORDERS_PATH / str(order_id)
    return json.load(open(file_name)) if file_name.exists() else {}

def save_order(order_id, order):
    file_name = ORDERS_PATH / str(order_id)
    if not order and file_name.exists():
        os.remove(file_name)
        return
    if order:
        open(file_name, 'w').write(json.dumps(order))
