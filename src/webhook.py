# std
from datetime import datetime, timedelta
import json

# deps
from flask import Blueprint, request, render_template
from loguru import logger
from requests import get, post, delete

# app
from mail import schedule_email
import storage
import rentle

webhook_endpoints = Blueprint("webhook", __name__, template_folder="/data")


@webhook_endpoints.route("/", methods=["POST"])
def webhook():
    data = request.json
    logger.debug(json.dumps(data))
    state = data["data"]["state"]
    bikes = []
    accessories = []
    delivery = False
    items = data["data"]["items"]
    order_id = data["data"]["orderNumber"]
    # extract the bike name
    for item in items:
        product_id = item['productId']
        category = rentle.get_category(product_id)
        thing = {'type': item['name']['def']}
        if category == 'accessories':
            accessories.append(thing)
        elif category == 'delivery':
            delivery = True
        elif category == 'bike':
            if "itemCodes" in item:
                for name in item["itemCodes"]:
                    if name:
                        thing['name'] = name
                        if code := rentle.get_bike_code(name):
                            thing['code'] = code 
            if thing['type'] in ['Taxi e-Bike', 'Family e-Bike'] and not 'name' in thing:
                # if no name and this type, wait for a named bike to be assigned
                if storage.get_order(order_id):
                    storage.delete_order(order_id)
                logger.info('Ignoring because at least one bike is missing a name assignment.')
                return 'thanks 🪝'
            bikes.append(thing)

    start_date_str = data["data"]["startDate"]
    start_date = datetime.strptime(start_date_str, "%Y-%m-%dT%H:%M:%S.%fZ")
    end_date_str = data["data"]["endDate"]
    end_date = datetime.strptime(start_date_str, "%Y-%m-%dT%H:%M:%S.%fZ")
    multiday = end_date.day != start_date.day
    phone_number = data["data"]["customerDetails"]["phone"]
    first_name = data["data"]["customerDetails"]["firstName"]
    last_name = data["data"]["customerDetails"]["lastName"]
    email = data["data"]["customerDetails"]["email"]
    logger.debug(f"bikes: {bikes}")
    logger.debug(f"accessories: {accessories}")
    logger.debug(f"delivery: {delivery}")
    logger.debug(f"start_date: {start_date}")
    logger.debug(f"phone_number: {phone_number}")
    if phone_number not in ["+4544110550", "+4526807960"]:
        logger.error("phone number not whitelisted")
        return "thanks"

    info = {
        "phone_number": phone_number,
        "first_name": first_name,
        "last_name": last_name,
        "order_id": order_id,
        "email": email,
        "bikes": bikes,
        "accessories": accessories,
        "delivery": delivery,
        "multiday": multiday
    }

    if state == "cancelled":
        logger.info(f"Order {order_id} was cancelled, so no new email is scheduled")
        if storage.get_order(order_id):
            storage.delete_order(order_id)
        return "thanks"

    send_time = max(
        datetime.now().timestamp() + 5,
        (start_date - timedelta(hours=12)).timestamp(),
    )

    schedule_email(
        order_id,
        email,
        render_template("email_header", **info),
        render_template("email_text", **info),
        send_time
    )
    logger.info(f"Scheduled email for order {order_id} to be sent at {send_time}")

    return "thanks 👌"
