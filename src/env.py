#std
from os import getenv
from pathlib import Path
import yaml

EMAIL_TEXT_PATH = Path('/data/email_text')
EMAIL_HEADER_PATH = Path('/data/email_header')
SMS_TEXT_PATH = Path('/data/sms_text')

ORDERS_PATH = Path('/data/orders')
if not ORDERS_PATH.exists():
    ORDERS_PATH.mkdir(exist_ok=True)

RENTLE_API_ID = getenv("RENTLE_API_ID")
if not RENTLE_API_ID:
    logger.error("Missing environment variable RENTLE_API_ID ")
    exit()

RENTLE_API_SECRET = getenv("RENTLE_API_SECRET")
if not RENTLE_API_SECRET:
    logger.error("Missing environment variable RENTLE_API_SECRET ")
    exit()

GATEWAYAPI_TOKEN = getenv("GATEWAYAPI_TOKEN")
if not GATEWAYAPI_TOKEN:
    logger.error("Missing environment variable GATEWAYAPI_TOKEN ")
    exit()

GATEWAYAPI_TOKEN = getenv("GATEWAYAPI_TOKEN")
if not GATEWAYAPI_TOKEN:
    logger.error("Missing environment variable GATEWAYAPI_TOKEN ")
    exit()

GMAIL_EMAIL = getenv("GMAIL_EMAIL")
if not GMAIL_EMAIL:
    logger.error("Missing environment variable GMAIL_EMAIL ")
    exit()

GMAIL_APP_PASSWORD = getenv("GMAIL_APP_PASSWORD")
if not GMAIL_APP_PASSWORD:
    logger.error("Missing environment variable GMAIL_APP_PASSWORD ")
    exit()
