#ib
from flask import Blueprint, render_template, request
from loguru import logger

#app
from env import SMS_TEXT_PATH, EMAIL_TEXT_PATH, EMAIL_HEADER_PATH

admin_endpoints = Blueprint('admin', __name__, url_prefix="/admin", template_folder='templates')

@admin_endpoints.route('/')
def admin():
    return render_template('admin.html')

@admin_endpoints.route('/edit_email')
def edit_email():
    email_text = open(EMAIL_TEXT_PATH).read() if EMAIL_TEXT_PATH.exists() else ""
    email_header = open(EMAIL_HEADER_PATH).read() if EMAIL_HEADER_PATH.exists() else ""
    return render_template('edit.html', email=True, text=email_text, header=email_header)

@admin_endpoints.route('/edit_email', methods=["post"])
def save_email():
    logger.info("email text/header updated") 
    text = request.form.get('text', '')
    header = request.form.get('header', '')
    open(EMAIL_TEXT_PATH, 'w').write(text)
    open(EMAIL_HEADER_PATH, 'w').write(header)
    return "Saved new email text 👍"

@admin_endpoints.route('/log')
def log():
    log_lines = open('/log').read().split('\n')
    return '<br>'.join(log_lines)
