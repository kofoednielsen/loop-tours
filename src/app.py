# lib
from flask import Flask
from loguru import logger

#app
from admin import admin_endpoints
from webhook import webhook_endpoints

logger.add("/log", level="TRACE", rotation="30 MB")

app = Flask(__name__)
app.register_blueprint(admin_endpoints)
app.register_blueprint(webhook_endpoints)
