#std
import smtplib, ssl
from email.message import EmailMessage

#lib
from loguru import logger

#app
from env import GMAIL_EMAIL, GMAIL_APP_PASSWORD
import storage
from datetime import datetime

def email_cron_job():
    order_ids = storage.get_all_order_ids()
    now = datetime.now().timestamp()
    for order_id in order_ids:
        order = storage.get_order(order_id)
        if order['send_time'] <= now:
            logger.info(f"Sending scheduled email for order id {order_id} to {order['email']}")
            send_email(order['email'], order['subject'], order['body'])
            storage.delete_order(order_id)

def schedule_email(order_id, email, subject, body, send_time):
    order = {
        'send_time': send_time,
        'email': email,
        'body': body,
        'subject': subject
    }
    storage.save_order(order_id, order)

def send_email(email, subject, body):
    msg = EmailMessage()
    msg.set_content(body, subtype='html')
    msg['Subject'] = subject
    msg['From'] = GMAIL_EMAIL
    msg['To'] = email
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
        server.login(GMAIL_EMAIL, GMAIL_APP_PASSWORD)
        server.send_message(msg, from_addr=GMAIL_EMAIL, to_addrs=email)

    logger.info(f"Sent email to {email}")

if __name__ == '__main__':
    import schedule
    import time

    schedule.every(1).minutes.do(email_cron_job)

    while True:
        schedule.run_pending()
        time.sleep(1)
