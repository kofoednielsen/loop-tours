#lib
from requests import get

from loguru import logger

#app
from env import (
    RENTLE_API_ID,
    RENTLE_API_SECRET,
)

categoryMapping = {
    'mqhL3RvdolYzMN9Sxpdc': 'accessories',
    'gvtkCsbmWMpJ7M6vpMhN': 'bike',
    'XqyBeoBQV73e7RMHuaee': 'delivery'
}

def get_category(product_id):
    res = get(
        f"https://api.rentle.io/admin/products/{product_id}",
        auth=(RENTLE_API_ID, RENTLE_API_SECRET),
        headers={"x-rentle-version": "2023-02-01"},
    )
    json = res.json()

    if 'categoryIds' in json:
        categoryIds = json['categoryIds']
        if not categoryIds or categoryIds[0] not in categoryMapping:
            return None
        else:
            return categoryMapping[categoryIds[0]]
        

def get_bike_code(bike_name):
    try:
        res = get(
            "https://api.rentle.io/admin/inventory-articles",
            auth=(RENTLE_API_ID, RENTLE_API_SECRET),
            headers={"x-rentle-version": "2023-02-01"},
        )
        json = res.json()

        items = json["data"]
        bike = next(filter(lambda i: bike_name in i["itemCodes"], items))
        return bike["fieldValues"]["Dz5z86OZqZUTvVErI1po"]
    except:
        return None


