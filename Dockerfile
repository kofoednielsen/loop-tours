FROM python:3.10-slim-buster

RUN pip install flask gunicorn pyyaml loguru requests schedule

WORKDIR /app
COPY src /app

CMD ["gunicorn", "-b", "0.0.0.0:8080", "app:app"]
